package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type ImgUrls struct {
	Idx  string
	Urls []string
}

type ChapUrl struct {
	Url  string
	Chap string
}

type HomeMeta struct {
	Tot   int
	Chaps []ChapUrl
}

var parser Parser

type Parser struct {
	HomeSel string
	ChapSel string
	HomeUrl string

	root_url   string
	tmp_dir    string
	res_dir    string
	home_file  string
	chaps_file string
}

func (p *Parser) SetTarget(HomeSel, ChapSel, HomeUrl, outDir string) {
	p.HomeSel = HomeSel
	p.ChapSel = ChapSel
	p.HomeUrl = HomeUrl

	parts := strings.Split(HomeUrl, "/")
	targetName := parts[len(parts)-1]
	work_dir := outDir + "/" + targetName

	p.root_url = "https://" + parts[2]

	p.tmp_dir = work_dir + "/.tmp"
	p.res_dir = work_dir
	p.home_file = p.tmp_dir + "/home.html"
	p.chaps_file = p.tmp_dir + "/chaps.json"
}

func (p *Parser) ParseHome() []ChapUrl {
	if force_html_dl {
		os.Remove(p.home_file)
		os.Remove(p.chaps_file)
	}

	var hm HomeMeta
	if err := read_json_if_available(p.chaps_file, &hm); err == nil {
		return hm.Chaps
	}

	make_dirs(p.tmp_dir)

	html, err := read_file_or_download(p.home_file, p.HomeUrl)
	check(err)
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(html))
	check(err)

	nodes := doc.Find(p.HomeSel)
	chaps_len := nodes.Length()
	chaps := make([]ChapUrl, chaps_len)
	digits_nbr := 4
	for i := 0; i < chaps_len; i++ {
		chap_idx := chaps_len - i - 1
		url, _ := nodes.Eq(chap_idx).Attr("href")
		if strings.HasPrefix(url, "/") {
			url = p.root_url + url
		}
		chap := int_to_str(i, digits_nbr)
		chaps[i].Url = url
		chaps[i].Chap = chap
	}
	hm.Chaps = chaps
	hm.Tot = chaps_len
	json_to_file(hm, p.chaps_file)
	return hm.Chaps
}

func (p Parser) ParseChap(chap ChapUrl) (ImgUrls, error) {
	chap_dir := p.tmp_dir + "/" + chap.Chap
	html_file := chap_dir + "/index.html"
	meta_file := chap_dir + "/meta.json"

	var img_url ImgUrls
	if err := read_json_if_available(meta_file, &img_url); err == nil {
		return img_url, nil
	}

	make_dirs(chap_dir)
	data, err := read_file_or_download(html_file, chap.Url)
	if err != nil {
		return img_url, err
	}

	var urls []string

	if strings.HasPrefix(p.ChapSel, "!kl") {
		urls = p.parseHtmlKl(data)
	} else {
		urls = p.parseHtmlWithXPath(data)
	}

	if len(urls) == 0 {
		die("unable to extract urls from: " + chap.Url + " double check html")
	}

	img_url.Idx = chap.Chap
	img_url.Urls = urls

	json_to_file(img_url, meta_file)
	return img_url, err
}

func matchTarget(buf []byte, i int, target []byte) bool {
	if buf[i] != target[0] || buf[i+len(target)-1] != target[len(target)-1] {
		return false
	}
	for j := 1; j < len(target)-1; j++ {
		if buf[i+j] != target[j] {
			return false
		}
	}
	return true
}

func (p *Parser) parseHtmlKl(buf []byte) []string {
	i := 0
	target := []byte(p.ChapSel)[3:]

	for ; i < len(buf); i++ {
		if matchTarget(buf, i, target) {
			// will probably crash if nothing is found
			break
		}
	}
	i += len(target)
	for buf[i] != 'h' {
		i++
	}
	start := i
	for buf[i] != '"' {
		i++
	}
	urls := string(buf[start:i])
	return strings.Split(urls, ",")
}

func (p *Parser) parseHtmlWithXPath(data []byte) []string {
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		return nil
	}
	nodes := doc.Find(p.ChapSel)
	var exist bool
	var src_url string
	urls := make([]string, len(nodes.Nodes))

	for i := range nodes.Nodes {
		node := nodes.Eq(i)
		src_url, exist = node.Attr("data-src")
		if exist {
			goto SET_URL_VALUE
		}
		src_url, exist = node.Attr("src")
		if !exist {
			fmt.Println("idx ", i, " does not have a valid url")
			continue
		}
	SET_URL_VALUE:
		if strings.HasPrefix(src_url, "/") {
			src_url = p.root_url + src_url
		}
		urls[i] = strings.Trim(src_url, "\t\n ")
	}
	return urls
}
