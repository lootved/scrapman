package main

func get_css_style() string {
	return `
	<style>
		 img {
		  display: block;
		  margin-left: auto;
		  margin-right: auto;
		  width: 95%;
		}
		
		button {
    	  border-radius: 1em;
		  margin-right: 1em;
		  width: 5em;
    	  height: 4em;
		  color: white;
		}
		
		.previous {
		  background-color: #ff0000;
		}

		.next {
		  background-color: #04AA6D;
		}
		
		.save {
		  color: black;
	    }

	</style>
`
}
