module scrapman

go 1.22.3

require (
	github.com/PuerkitoBio/goquery v1.9.2
)

require gitlab.com/lootved/progoress v0.0.0

replace gitlab.com/lootved/progoress => ../progoress

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	golang.org/x/net v0.24.0 // indirect
)
