document.addEventListener("keydown", function (event) {
  switch (event.key) {
    case "ArrowLeft":
      document.querySelector("a.prev").click();
      break;
    case "ArrowRight":
      document.querySelector("a.next").click();
      break;
    case "n":
      fetch("/next")
        .then((response) => response.text())
        .then((next) => window.location.href = `/${next}/index.html`);
      break;
    case "s":
      const val = document.querySelector("a.next").href.split("/")[3];
      saveNext(val);
      document.querySelectorAll("button.save").forEach(b => b.setAttribute("class", "prev"))
      alert("saved " + val);
  }
});

function saveNext(val) {
  fetch("/?next=" + val, {
    method: "POST",
  });
}
