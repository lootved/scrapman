package main

import (
	"flag"
	"io/fs"
	"os"
	"strings"
)

func main() {

	dir := "."
	n := 3
	start := -1

	flag.StringVar(&dir, "d", dir, "resources directory to clean")
	flag.IntVar(&n, "n", n, "number of folders to cleanup")
	flag.IntVar(&start, "s", start, "index of first folder to cleanup")
	flag.Parse()

	if start == -1 {
		exit("missing mandatory flag -s <idx-of-first-folder>")
	}

	dirs, err := os.ReadDir(dir)
	check(err)
	if start >= len(dirs) {
		exit("invalid start flag, exeeds total length")
	}
	dirs = dirs[start:]
	dirs = dirs[:min(len(dirs), n)]
	if len(dirs) == 0 {
		exit("all dirs were ignored")
	}
	n = len(dirs)
	print("will clean ", n, " folders from ", dirs[0].Name())
	if n > 1 {
		print(" to ", dirs[n-1].Name())
	}
	println()

	rmed := false
	for _, d := range dirs {
		rmed = cleanup(dir, d) || rmed
	}
	if rmed {
		println("some dirs were cleaned up")
	} else {
		println("nothing to do")
	}
}

func cleanup(dir string, d fs.DirEntry) bool {
	name := d.Name()
	if !d.IsDir() || name[0] == '.' {
		return false
	}
	path := dir + "/" + name
	dirs, err := os.ReadDir(path)
	check(err)
	rmed := false
	for _, d := range dirs {
		name := d.Name()
		if d.IsDir() || !strings.HasSuffix(name, ".jpg") {
			continue
		}
		f, err := d.Info()
		check(err)
		if f.Size() == 0 {
			os.Remove(path + "/" + name)
			rmed = true
			// println("rmed ", path+"/"+name)
		}
	}
	if rmed {
		os.Remove(path + "/index.html")
	}
	return rmed
}

func check(err error) {
	if err != nil {
		exit(err.Error())
	}
}

func exit(msg string) {
	println(msg)
	os.Exit(1)
}
