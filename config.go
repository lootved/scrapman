package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/user"
	"strconv"
	"strings"
)

type Config struct {
	TargetGroup []TargetGroup        `json:"targetGroup,omitempty"`
	Urls        []string             `json:"urls,omitempty"`
	Targets     []Target             `json:"targets"`
	Sels        map[string]Selectors `json:"sels"`
	Path        string               `json:"path,omitempty"`
}

type Selectors struct {
	Home string `json:"home"`
	Chap string `json:"chap"`
}

type TargetGroup struct {
	Idx     int    `json:"idx"`
	Comment string `json:"comment,omitempty"`
	Targets []int  `json:"targets"`
}

type Target struct {
	Idx     int    `json:"idx"`
	Comment string `json:"comment,omitempty"`
	Url     string `json:"url"`
	Date    string `json:"date,omitempty"`
	Last    string `json:"last,omitempty"`
}

var do_debug = false
var n_chaps_to_ignore = 0
var nbr_chaps = 200
var force_html_generation = false
var force_html_dl = false
var dryrun = false
var config_is_immutable = false
var targetNums = ""
var tgIdx = -1
var targetRange = ""
var globalConf Config
var output_dir = "out"
var urls_to_targets = false
var max_chap_in_parallel = 2

var dl_only = false

var conf_path = "~/hn/urls.json"

func ReadConf() {

	var fdry = flag.Bool("fdry", false, "equivalent to  -f -dry")

	flag.StringVar(&conf_path, "p", conf_path, "config path")
	flag.StringVar(&targetRange, "tr", targetRange, "comma separated [start,end] range")

	flag.IntVar(&n_chaps_to_ignore, "i", 0, "number of chapters to ignore")
	flag.IntVar(&nbr_chaps, "n", nbr_chaps, "maximum number of chapters to download")
	flag.BoolVar(&force_html_dl, "f", false, "force redownloading home page")
	flag.BoolVar(&force_html_generation, "fhg", force_html_generation, "force html generation for each chapter")
	flag.BoolVar(&do_debug, "d", do_debug, "enable debugging")
	flag.BoolVar(&dryrun, "dry", dryrun, "will only print what will be downloaded")
	flag.BoolVar(&urls_to_targets, "utt", urls_to_targets, "move urls to target in the config file")
	flag.StringVar(&targetNums, "t", targetNums, "comma separated list of targets to process")
	flag.IntVar(&tgIdx, "tg", tgIdx, "target group to process")
	flag.StringVar(&output_dir, "o", output_dir, "base output folder")

	flag.IntVar(&max_chap_in_parallel, "mc", max_chap_in_parallel, "nbr of chapter to dll in parallel")
	flag.BoolVar(&config_is_immutable, "ic", config_is_immutable, "make config immutable")
	flag.BoolVar(&dl_only, "dl", dl_only, "download html only")

	var conf_here = flag.Bool("h", false, "use ./urls.json as conf file")
	var hf_flag = flag.Bool("hf", false, "equivalent to -h -f")
	var last_target = flag.Bool("l", false, "select last target")
	// var gen_sum_html = flag.Bool("gsh", false, "generate combined html")
	flag.Parse()
	if *hf_flag {
		*conf_here = true
		force_html_dl = true
	}

	if *conf_here {
		conf_path = "urls.json"
	}

	if *fdry {
		dryrun = true
		force_html_dl = true
	}
	data := read_file(conf_path)
	err := json.Unmarshal(data, &globalConf)
	check(err)
	if *last_target {
		targetNums = strconv.Itoa(len(globalConf.Targets) - 1)
	}
	_, ok := globalConf.Sels["default"]
	if !ok {
		die("'default' selector must be provided in the config file")
	}
	updateOutputDir()
}

func (t Target) GetLastChapter() int {
	s := t.Last
	i := 0
	for i < len(s) && s[i] == '0' {
		i++
	}
	s = s[i:]
	i = 0
	for i < len(s) && s[i] >= '0' && s[i] <= '9' {
		i++
	}
	if i == 0 {
		return 0
	}
	res, _ := strconv.Atoi(s[:i])
	return res
}

func updateOutputDir() {
	if globalConf.Path != "" && output_dir == "out" {
		output_dir = globalConf.Path
	}
	if strings.HasPrefix(output_dir, "~/") {
		usr, _ := user.Current()
		output_dir = usr.HomeDir + output_dir[1:]
	} else if !strings.HasPrefix(output_dir, "/") {
		pwd, _ := os.Getwd()
		output_dir = pwd + "/" + output_dir
	}
}

func GetTargets() []*Target {
	if targetNums == "" && tgIdx == -1 && targetRange == "" {
		die("must provide at least a target of a target group")
	}
	targetIdxs := make([]int, 0)

	if targetNums != "" {
		idxs := strings.Split(targetNums, ",")
		for _, idx := range idxs {
			val, err := strconv.Atoi(idx)
			if err != nil {
				fmt.Println("[warn] unable to parse int from list of targets")
			} else {
				targetIdxs = append(targetIdxs, val)
			}
		}
	} else if targetRange != "" {
		start, end := parseTargetRange()
		for i := start; i <= end; i++ {
			targetIdxs = append(targetIdxs, i)
		}
	} else {
		for _, idx := range globalConf.TargetGroup[tgIdx].Targets {
			targetIdxs = append(targetIdxs, idx)
		}
	}
	res := make([]*Target, len(targetIdxs))
	for i, idx := range targetIdxs {
		if idx >= len(globalConf.Targets) {
			die("target index overflow " + strconv.Itoa(idx))
		}
		res[i] = &globalConf.Targets[idx]
		res[i].Idx = idx
		if !strings.HasPrefix(res[i].Url, "http") {
			fmt.Println("invalid target '", res[i].Url, "', no url was found double check config file")
			os.Exit(1)
		}
	}
	return res
}

func parseTargetRange() (int, int) {
	idxs := strings.Split(targetRange, ",")
	if len(idxs) != 2 {
		die("invalid target range, must be in format start,end")
	}
	start, err := strconv.Atoi(idxs[0])
	if err != nil {
		die("unable to parse start from target range")
	}
	end, err := strconv.Atoi(idxs[1])
	if err != nil {
		die("unable to parse end from target range")
	}
	end = min(end, len(globalConf.Targets)-1)
	return start, end
}
