package main

import (
	_ "embed"
	"encoding/base64"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func generate_chap_html(res_dir, chap_nbr, title string) {
	idx, _ := strconv.Atoi(chap_nbr)
	next := int_to_str(idx+1, len(chap_nbr))
	// doesn't correctky handle index 0, can be ignored
	previous := int_to_str(idx-1, len(chap_nbr))

	chap_dir := res_dir + "/" + chap_nbr
	files, err := os.ReadDir(chap_dir)
	check(err)
	imgs := make([]string, len(files))
	size := 0
	for _, file := range files {
		name := file.Name()
		if strings.HasSuffix(name, ".jpg") {
			img := generate_one_page_html(name)
			imgs[size] = img
			size++
		}
	}
	template := `
<!DOCTYPE html>
<html>
  <head>
      <title>` + title + `</title>
  </head>
  ` + get_css_style()
	template += "\n  <body>\n"
	template += javascript()
	template += previous_and_next_buttons(previous, next)
	for i := 0; i < size; i++ {
		template += imgs[i] + "\n    "
		template += `<div style="text-align: center;">` + strconv.Itoa(i+1) + "/" + strconv.Itoa(size) + "</div>\n    "
	}

	template += previous_and_next_buttons(previous, next)
	template += "\n  <br>\n  <br>.\n"

	template += "  </body>\n</html>"

	save_file(template, chap_dir+"/index.html")
}

func previous_and_next_buttons(previous, next string) string {
	return `  <div style="text-align: center;">
      ` + linkButton("previous", previous) + `
      <button class="save" onclick="saveNext('` + next + `')"> save</button>
      ` + linkButton("next", next) + `
  </div>`
}

func linkButton(class, target string) string {
	cls := class[:4]
	href := "../" + target + "/index.html"
	return `<a href="` + href + `" class="` + cls + `"> <button class="` + class + `">` + class + `</button></a>`
}

func generate_html_showing_all_dir(src_path string) {
	files, err := os.ReadDir(src_path)
	check(err)

	items := make([]string, 0)

	for _, file := range files {
		if file.IsDir() {
			url := `<li><a href="` + file.Name() + `/index.html">`
			url += file.Name() + "</a></li>"
			items = append(items, url)
		}
	}

	template := `
<!DOCTYPE html>
<html>
    <head>
        <title>Anm</title>
    </head>
    
	`
	size := len(items)
	template += "<div>\n    "
	for i := 0; i < size-1; i++ {
		if (i+1)%10 == 0 {
			template += "</div><br>\n    <div>\n    "
			template += "<hr>\n    "
		}
		template += items[i] + "\n    "
	}

	template += items[size-1] + "\n    </div>\n"

	template += "    <br>\n    <br>\n</html>"

	dst_path := src_path + "/index.html"
	save_file(template, dst_path)

}
func generate_one_page_html(img_path string) string {
	return `<div class="img-chap-div">
      <img src="` + img_path + `" >
  </div>`
	// can also embed as base64 for more portability
	raw := read_file(img_path)
	b64_img := base64.StdEncoding.EncodeToString(raw)
	return `<div>
<img id="image-0" src="data:image/jpg;base64,` + b64_img + `" >
</div>`
}

//go:embed js/main.js
var jscode string

func javascript() string {
	return fmt.Sprintf(`
    <script>
%s
    </script>
`, jscode)

}
