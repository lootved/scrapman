package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

func (s Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		s.fs.ServeHTTP(w, r)
		return
	}
	next := r.URL.Query().Get("next")
	if next == "" {
		w.WriteHeader(400)
		return
	}
	os.WriteFile(s.nextPath, []byte(next), 0644)
	w.WriteHeader(200)
}

func main() {

	port := "80"
	dir := "."
	pub := false

	flag.StringVar(&port, "p", port, "port to listen to")
	flag.StringVar(&dir, "d", dir, "directory to serve")
	flag.BoolVar(&pub, "pub", pub, "serve on a public interface")
	flag.Parse()

	srv := newServer(dir)
	http.Handle("/", &srv)

	itf := "127.0.0.1"
	if pub {
		itf = "0.0.0.0"
	}
	addr := itf + ":" + port
	fmt.Println("Listening on ", addr)
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		// if err == http.
		fmt.Println("sudo setcap 'cap_net_bind_service=+ep' <path/to/binary> to allow bind to low port")
		fmt.Println(err)
		os.Exit(1)
	}
}

func newServer(dir string) Server {
	nextPath := dir + "/next"
	s := Server{nextPath: nextPath}
	s.fs = http.FileServer(http.Dir(dir))
	return s
}

type Server struct {
	fs       http.Handler
	nextPath string
}
