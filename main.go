package main

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"gitlab.com/lootved/progoress"
)

var pm *progoress.ProgressMonitor

var config_was_updated = false

var received_sigkill = false

func main() {
	ReadConf()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		received_sigkill = true
	}()

	if urls_to_targets {
		moveUrlsToTargets()
		return
	}
	targets := GetTargets()
	day := time.Now().Format("2006-01-02")
	for _, target := range targets {
		ProcessTarget(target, day)
	}
	if !config_is_immutable && config_was_updated {
		fmt.Println("config wll be updated:", conf_path)
		json_to_file(globalConf, conf_path)
	}
	// generate_html_showing_all_dir(OUT_DIR)
}

func ProcessTarget(target *Target, day string) {

	parts := strings.Split(target.Url, "/")
	baseUrl := parts[2]
	BASE_URL = ""
	sel, ok := globalConf.Sels[baseUrl]
	if !ok {
		fmt.Println("[warn] using default selectors")
		sel = globalConf.Sels["default"]
	}
	debug("selectors for target ", baseUrl, " ", sel)

	parser.SetTarget(sel.Home, sel.Chap, target.Url, output_dir)

	chapsToIgnore := n_chaps_to_ignore
	if chapsToIgnore == 0 && target.Last != "" {
		chap := target.GetLastChapter() + 1
		fmt.Println("will skip ", chap, " chapters")
		if chap != 0 {
			chapsToIgnore = chap
		}
	}

	chaps := parser.ParseHome()
	allChaps := chaps
	if len(chaps) <= chapsToIgnore {
		fmt.Println("all chapters were ignored, nothing to process for ", target.Url)
		return
	}

	chaps = chaps[chapsToIgnore:]
	chaps_len := min(len(chaps), nbr_chaps)
	chaps = chaps[:chaps_len]

	fmt.Println("will download ", len(chaps), " chaps to folder ", output_dir)
	fmt.Println("from ", chaps[0].Url)

	fmt.Println("to ", chaps[len(chaps)-1].Url)

	if dryrun {
		os.Exit(0)
	}

	BASE_URL = "https://" + baseUrl
	parseAllChaps(chaps)
	debug("meta is extracted")
	metadata := make([]ImgUrls, 0)

	for _, chap_url := range chaps {
		var img_url ImgUrls
		read_json(parser.tmp_dir+"/"+chap_url.Chap+"/meta.json", &img_url)
		metadata = append(metadata, img_url)
	}

	debug("starting downloading chapters ...")

	sem := make(chan bool, max_chap_in_parallel)
	var wg sync.WaitGroup
	size := len(metadata)
	pm = progoress.NewProgressMonitor(size)
	if !do_debug {
		pm.StartMonitoring()
	}
	for i, meta := range metadata {
		out_dir := parser.res_dir + "/" + meta.Idx
		done_file := out_dir + "/index.html"
		if !path_exists(done_file) {
			wg.Add(1)
			sem <- true // would block if sem channel is already filled
			if received_sigkill {
				println("received sigkill stopping after")
				wg.Done()
				chaps = chaps[:i]
				break
			}
			go func(meta ImgUrls, i int) {
				chapUrl := allChaps[i+chapsToIgnore].Url
				title := extractTitle(chapUrl)
				download_img_from_meta(meta, parser.res_dir, i)
				generate_chap_html(parser.res_dir, meta.Idx, title)
				<-sem // free a resource
				wg.Done()
			}(meta, i)
		} else {
			pm.StartTask(i, 1)
			pm.FinishSubTask(i)
			chapUrl := allChaps[i+chapsToIgnore].Url
			title := extractTitle(chapUrl)
			if force_html_generation {
				generate_chap_html(parser.res_dir, meta.Idx, title)
			}
		}
	}
	config_was_updated = true
	chap := chaps[len(chaps)-1]
	target.Last = chap.Chap + "-" + extractChapNbrs(chap.Url)
	target.Date = day
	wg.Wait()
	//
	// generate_html_showing_all_dir(parser.res_dir)
}

func extractTitle(url string) string {
	url = strings.TrimSuffix(url, "/")
	parts := strings.Split(url, "/")
	suffix := parts[len(parts)-1]
	start := 0
	for start < len(suffix) && (suffix[start] < '0' || suffix[start] > '9') {
		start++
	}
	if start == len(suffix) {
		return "not-found"
	}
	end := start
	for end < len(suffix) && (suffix[end] >= '0' && suffix[end] <= '9') {
		end++
	}
	if end < len(suffix) && (suffix[end] == '.' || suffix[end] == '-') {
		end++
	}
	for end < len(suffix) && (suffix[end] >= '0' && suffix[end] <= '9') {
		end++
	}

	return suffix[start:end]
}

func download_img_from_meta(meta ImgUrls, res_dir string, idx int) {
	dst := res_dir + "/" + meta.Idx + "/"
	make_dirs(dst)
	var size = len(meta.Urls)
	digits_nbr := nbr_of_digits(size)

	pm.StartTask(idx, len(meta.Urls))
	for i, url := range meta.Urls {
		nbr := int_to_str(i, digits_nbr)
		out := dst + nbr + ".jpg"
		if !path_exists(out) {
			debug("downloading ", meta.Idx, ":", i+1, "/", size)
			download(url, out, true)
		}
		pm.FinishSubTask(idx)
	}
	//run_shell_cmd("convert " + dst + "*.jpg " + dst + "output.pdf")
}

func parseAllChaps(chaps []ChapUrl) {
	max_running := 10
	sem := make(chan bool, max_running)
	var wg sync.WaitGroup
	fmt.Println("will download html from ", len(chaps), " chaps then extract img url")
	wg.Add(len(chaps))
	failed := false
	for i := range chaps {
		sem <- true
		go func(chap ChapUrl) {
			// extract_metadata(chap)
			_, err := parser.ParseChap(chap)
			if err != nil {
				failed = true
			}
			<-sem // free a resource
			wg.Done()
		}(chaps[i])
	}
	wg.Wait()
	if failed {
		println("failed to parse one of the chaps ...")
		os.Exit(0)
	}
}

func moveUrlsToTargets() {
	urls := globalConf.Urls
	globalConf.Urls = make([]string, 0)
	i := 0
	for ; i < len(globalConf.Targets); i++ {
		globalConf.Targets[i].Idx = i
	}
	if output_dir == "out" {
		output_dir = ""
	}
	for _, url := range urls {
		target := Target{Idx: i, Url: url}
		globalConf.Targets = append(globalConf.Targets, target)
		i++
	}
	dst := conf_path
	if !force_html_dl {
		dst += ".n"
		fmt.Println("to update the config: mv", dst, conf_path)
	}
	fmt.Println("saving config to ", dst)
	json_to_file(globalConf, dst)
}

func extractChapNbrs(url string) string {
	url = strings.TrimSuffix(url, "/")
	i := 0
	parts := strings.Split(url, "/")
	s := parts[len(parts)-1]

	for i < len(s) && (s[i] < '0' || s[i] > '9') {
		i++
	}
	if i >= len(s) {
		return "0-invalid"
	}
	s = s[i:]
	i = 0
	for i < len(s) && s[i] >= '0' && s[i] <= '9' {
		i++
	}
	if i+2 > len(s) || s[i+1] < '0' || s[i+1] > '9' {
		return s[:i]
	}
	i += 2
	for i < len(s) && s[i] >= '0' && s[i] <= '9' {
		i++
	}
	return s[:i]
}
