package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"
)

func make_dirs(dir string) {
	if !path_exists(dir) {
		err := os.MkdirAll(dir, os.ModePerm)
		check(err)
	}
}

func path_exists(path string) bool {
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		return false
	}
	return true
}

var BASE_URL = ""

func download(uri string, path string, close bool) []byte {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", uri, nil)
	userAgent := "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.3"
	req.Header.Set("User-Agent", userAgent)
	if BASE_URL != "" {
		req.Header.Set("Referer", BASE_URL)

	}

	req.Header.Set("Sec-Fetch-Mode", "navigate")
	req.Header.Set("Sec-Fetch-Site", "none")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Sec-Gpc", "1")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Upgrade-Insecure-Requests", "1")
	req.Header.Set("Sec-Fetch-User", "?1")
	// req.Header.Set("Accept-Encoding", "gzip, deflate, br, zstd")
	req.Header.Set("Sec-Fetch-Dest", "document")
	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	req.Header.Set("Dnt", "1")
	req.Header.Set("Priority", "u=0, i")

	resp, err := client.Do(req)
	check(err)
	if close {
		defer resp.Body.Close()
	}
	if resp.StatusCode != 200 {
		die("returned non 200 status code from " + uri)
	}
	data, err := io.ReadAll(resp.Body)
	check(err)
	os.WriteFile(path, data, 0766)
	return data
}

func check(err error) {
	if err != nil {
		die(err.Error())
	}
}

func die(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}

func read_json_if_available(path string, v any) error {
	if path_exists(path) {
		debug(path, "already exists will not be recomputed")
		read_json(path, v)
		return nil
	}
	return errors.New("file does not exists")
}

func read_json(path string, MUST_BE_A_REFERENCE any) {
	json.Unmarshal(read_file(path), MUST_BE_A_REFERENCE)
}

func read_file(path string) []byte {
	if strings.HasPrefix(path, "~") {
		usr, _ := user.Current()
		path = usr.HomeDir + path[1:]
	}

	f, err := os.Open(path)
	check(err)
	data, err := io.ReadAll(f)
	check(err)
	return data
}

func json_to_file(obj interface{}, path string) {
	if strings.HasPrefix(path, "~") {
		usr, _ := user.Current()
		path = usr.HomeDir + path[1:]
	}
	f, _ := os.Create(path)
	defer f.Close()
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", " ")
	err := enc.Encode(obj)
	debug("Writing output to", path)
	check(err)
	f.Write(buf.Bytes())
}

func read_file_or_download(path string, url string) ([]byte, error) {
	if !path_exists(path) {
		return download(url, path, false), nil
	}
	r, _ := os.Open(path)
	defer r.Close()
	data, err := io.ReadAll(r)
	return data, err
}

func int_to_str(n int, digits_nbr int) string {
	nbr := strconv.Itoa(n)
	len := nbr_of_digits(n)
	prefix := ""
	for i := 0; i < digits_nbr-len; i++ {
		prefix += "0"
	}
	return prefix + nbr
}

func str_int_to_str(str string, digits_nbr int) string {
	idx, err := strconv.Atoi(str)
	if err == nil {
		return int_to_str(idx, digits_nbr)
	}
	return str
}

func save_file(content string, path string) {
	debug("Writing output to", path)
	err := os.WriteFile(path, []byte(content), 0766)
	check(err)
}

func nbr_of_digits(num int) int {
	n := num / 10
	digits_nbr := 1
	for n > 0 {
		digits_nbr++
		n /= 10
	}
	return digits_nbr
}

func run_shell_cmd(cmd string) {
	a := strings.Split(cmd, " ")
	err := exec.Command(a[0], a[1:]...).Run()
	if err != nil {
		debug("failed to run cmd:", cmd)
	}
}

func debug(a ...any) {
	if do_debug {
		fmt.Println(a...)
	}
}
